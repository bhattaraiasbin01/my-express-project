import { Schema } from "mongoose";
let playerGameSchema = Schema({
  matchName: {
    type: String,
  },
  noOfGoal: {
    type: String,
  },
});
let parentInfoSchema = Schema({
  fatherName: {
    type: String,
  },
  motherName: {
    type: String,
  },
}); // we create schema to define object
let playerSchema = Schema(
  {
    name: {
      type: String,
      // required: [true, "name field is required"],
    },
    age: {
      type: Number,
      // required: [true, "age field is required"],
    },
    noOfMatches: {
      type: String,
      // required: [true, "noOfMatches field is required"],
    },
    isMarried: {
      type: Boolean,
    },
    Spouse: {
      type: String,
    },

    gender: {
      type: String,
      default: "male",
    },
    parentInfo: parentInfoSchema,
    playerGame: [playerGameSchema],
    favFood: [{ type: String }],
  },
  { timestamps: true } //it gives created at and updated info
);

export default playerSchema;
