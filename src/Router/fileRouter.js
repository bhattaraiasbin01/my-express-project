import { Router } from "express";
import upload from "../MiddleWare/uploadFiles.js";

const fileRouter = Router();
fileRouter.route("/single").post(upload.single("file"), (req, res) => {
  console.log(req.file);
  res.json("hello");
});
fileRouter.route("/multiple").post(upload.array("files", 5), (req, res) => {
  console.log(req.files);
  res.json("multiple file");
});
export default fileRouter;
