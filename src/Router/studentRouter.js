import { Router } from "express";
import {
  createStudents,
  deleteStudentDetails,
  readallStudents,
  readStudentDetails,
  updateStudentDetails,
} from "../Controller/studentController.js";
import validation from "../MiddleWare/Validation.js";
import studentValidation from "../validations/studentValidation.js";

export const studentRouter = Router();

studentRouter
  .route("/")
  .post(validation(studentValidation), createStudents)
  .get(readallStudents);

studentRouter
  .route("/:id")
  .get(readStudentDetails)
  .patch(updateStudentDetails)
  .delete(deleteStudentDetails);
