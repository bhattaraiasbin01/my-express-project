// const expressAsyncHandler = require("express-async-handler");
import expressAsyncHandler from "express-async-handler";

import bcrypt from "bcrypt";

export let generateHashCode = expressAsyncHandler(async ({ text }) => {
  let hashCode = await bcrypt.hash(text, 10);
  return hashCode;
});
export let compareHashCode = expressAsyncHandler(async ({ text, hashCode }) => {
  let isValid = await bcrypt.compare(text, hashCode);
  return isValid;
});
