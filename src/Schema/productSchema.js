import { Schema } from "mongoose";
// we create schema to define object
let productSchema = Schema(
  {
    name: {
      type: String,
      trim: true,
    },
    quantity: {
      type: Number,
      trim: true,
    },
    price: {
      type: Number,
      trim: true,
    },
    featured: {
      type: Boolean,
      trim: true,
    },
    productImage: {
      type: String,
      trim: true,
    },

    manufactureDate: {
      type: Date,
      default: new Date(),
      trim: true,
    },

    company: {
      type: String,
      default: "apple",
      enum: ["apple", "samsung", "dell", "mi"],
      trim: true,
    },
  },
  { timestamps: true } //it gives created at and updated info
);

export default productSchema;
