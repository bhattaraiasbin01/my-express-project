import Joi from "joi";
const playerValidation = Joi.object()
  .keys({
    name: Joi.string().custom((value, msg) => {
      //cutsom is used to show custom error message that is likely to occur
      if (value.match(/^[a-zA-Z]+$/)) {
        return true;
      } else {
        return msg.message("Only alphabets is allowed");
      }
    }),
    age: Joi.number()
      .min(18)
      .max(60)
      // .required(),
      .custom((value, msg) => {
        //cutsom is used to show custom error message that is likely to occur
        let stringValue = String(value);

        if (stringValue.match(/^[0-9]+$/) || (value = "")) {
          return true;
        } else {
          return msg.message("Only alphabets is allowed");
        }
      }),
    noOfMatches: Joi.number().required(),
    isMarried: Joi.boolean().required(),
    spouse: Joi.string().when("isMarried", {
      is: true,
      then: Joi.required(),
      otherwise: Joi.optional().allow(),
    }),
    // spouse: Joi.string().optional().allow(""),
    gender: Joi.string().required().valid("male", "female", "others"),
    parentInfo: Joi.object().keys({
      fatherName: Joi.string().required(),
      // .custom((value, msg) => {
      //cutsom is used to show custom error message that is likely to occur
      //   if (value.match(/^[a-zA-Z]+$/)) {
      //     return true;
      //   } else {
      //     return msg.message("Only alphabets is allowed");
      //   }
      // }),
      motherName: Joi.string().required(),
      //custom((value, msg) => {
      //   //cutsom is used to show custom error message that is likely to occur
      //   if (value.match(/^[a-zA-Z]+$/)) {
      //     return true;
      //   } else {
      //     return msg.message("Only alphabets is allowed");
      //   }
      // }),
    }),
    playerGame: Joi.array().items(
      Joi.object().keys({
        matchName: Joi.string().required(),
        //   custom((value, msg) => {
        //   //cutsom is used to show custom error message that is likely to occur
        //   if (value.match(/^[a-zA-Z]+$/)) {
        //     return true;
        //   } else {
        //     return msg.message("Only alphabets is allowed");
        //   }
        // }),
        noOfGoal: Joi.number().required(),
      })
    ),
    favFood: Joi.array().items(Joi.string()).required(),
    // .custom((value, msg) => {
    //   //cutsom is used to show custom error message that is likely to occur
    //   if (value.match(/^[a-zA-Z]+$/)) {
    //     return true;
    //   } else {
    //     return msg.message("Only alphabets is allowed");
    //   }
    // }),
  })
  .unknown(false);

export default playerValidation;
