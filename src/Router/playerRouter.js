import { Router } from "express";
import {
  createPlayer,
  deletePlayer,
  readAllPlayers,
  readPlayerDetails,
  updatePlayerDetails,
} from "../Controller/playerController.js";
import abnormal from "../MiddleWare/abnormal.js";
import normal from "../MiddleWare/normalMiddleware.js";
import validation from "../MiddleWare/Validation.js";
import playerValidation from "../validations/playerValidation.js";

export const PlayerRouter = Router();

PlayerRouter.route("/")
  // .post(validation(playerValidation), normal, abnormal("nitan"), createPlayer)
  .post(createPlayer)
  .get(readAllPlayers);

PlayerRouter.route("/:id")

  .get(readPlayerDetails)
  .patch(updatePlayerDetails)
  .delete(deletePlayer);
