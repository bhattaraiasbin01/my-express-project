// console.log("I am server");
import express, { urlencoded } from "express";
import { json } from "express";
import { model, Schema } from "mongoose";

import { port } from "./src/config/config.js";
import connectDb from "./src/connectexpress/connectEpress.js";
import errorHandler from "./src/helper/errorHandler.js";
import { bookRouter } from "./src/Router/bookRouter.js";
import { firstRouter } from "./src/Router/firstRouter.js";
import { foodRouter } from "./src/Router/foodRouter.js";
import { PlayerRouter } from "./src/Router/playerRouter.js";
import productRouter from "./src/Router/productRouter.js";
import reviewRouter from "./src/Router/reviewRouter.js";
import { studentRouter } from "./src/Router/studentRouter.js";
import { sendMail } from "./src/utils/sendMail.js";
import fileRouter from "./src/Router/fileRouter.js";
import adminRouter from "./src/Router/adminRouter.js";

let expressApp = express();
expressApp.use(json());
expressApp.use(urlencoded({ extended: true }));
expressApp.use(express.static("./public"));

expressApp.use("/first", firstRouter);
expressApp.use("/food", foodRouter);
expressApp.use("/book", bookRouter);
expressApp.use("/students", studentRouter);
expressApp.use("/players", PlayerRouter);
expressApp.use("/product", productRouter);
expressApp.use("/reviews", reviewRouter);
expressApp.use("/uploads", fileRouter);
expressApp.use("/admins", adminRouter);

connectDb();

// let studentData = {
//   name: "Asbin",
//   roll: 1,
//   address: "morang",
// };
// try {
//   await Student.create(studentData);
// } catch (err) {
//   let error = new Error(err.message); //if new keyword is used next word 1st letter must be capital
//   throw error;
// }

// try {
//   let result = await Student.find({});
//   console.log(result);
// } catch (err) {
//   let error = new Error(err.message); //if new keyword is used next word 1st letter must be capital
//   throw error;
// }
// let updateData = {
//   name: "ram",
//   roll: 2,
//   address: "jhapa",
// };
// try {
//   await Student.findByIdAndUpdate("6411ab5c8822a14c5fc4e04c", updateData);
// } catch (err) {
//   let error = new Error(err.message); //if new keyword is used next word 1st letter must be capital
//   throw error;
// }
// try {
//   await Student.findByIdAndDelete("6411ab5c8822a14c5fc4e04c");
// } catch (err) {
//   let error = new Error(err.message); //if new keyword is used next word 1st letter must be capital
//   throw error;
// }
// try {
//   await sendMail({
//     from: '"underrated developer"',
//     to: ["bhattaraiasbin01@gmail.com"],
//     subject: "this is subject",
//     html: `<h1>Hello World<h1>`,
//   });
//   console.log("email is sent successfully");
// } catch (error) {
//   console.log("unable to send email");
// }
expressApp.use(errorHandler);

expressApp.listen(port, () => {
  console.log(`express app is listening at port ${port}`);
});
