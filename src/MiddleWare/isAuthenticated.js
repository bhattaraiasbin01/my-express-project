import expressAsyncHandler from "express-async-handler";
import { verifyToken } from "../utils/token.js";
import { secretKey } from "../config/config.js";

let isAuthenticated = expressAsyncHandler(async (req, res, next) => {
  let bearerToken = req.headers.authorization;
  let token = bearerToken.split(" ")[1];
  console.log(token);

  let info = await verifyToken({
    token: token,
    secretKey: secretKey,
  });

  req.info = info;

  next();
});

export default isAuthenticated;
