import { HttpStatus } from "../config/config.js";

const errorHandler = (err, req, res, next) => {
  let statusCode = err.statusCode || HttpStatus.INTERNAL_SERVER_ERROR;
  let message = err.message || "Internal server error";
  res.status(statusCode).json({
    success: false,
    message,
  });
  console.log("hello");
};
export default errorHandler;
