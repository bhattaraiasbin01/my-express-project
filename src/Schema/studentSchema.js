import { Schema } from "mongoose";

let studentSchema = Schema(
  {
    name: {
      type: String,
      // required: [true, "Name field is required"],
    },
    roll: {
      type: Number,
      // required: [true, "ROll field is required"],
    },
    address: {
      type: String,
      // required: [true, "Address field is required"],
    },
  },
  { timestamps: true } //it gives created at and updated info
);

export default studentSchema;
