import { Router } from "express";
import { foodRouterRead } from "../Controller/foodController.js";
import {
  foodMiddleware,
  foodMiddleware1,
  foodMiddleware2,
} from "../MiddleWare/foodMiddleWare.js";

export const foodRouter = Router();

foodRouter
  .route("/")
  .get(
    foodMiddleware,
    foodMiddleware1("admin"),
    foodMiddleware2,
    foodRouterRead
  );
// .get(foodMiddleware, (req,res,next)=>{}, foodMiddleware2, foodRouterRead);

foodRouter.route("/:foodName").get((req, res) => {
  res.json({
    foodName: req.params.foodName,
  });
});
foodRouter.route("/").post((req, res) => {
  res.json({
    food: req.body.foodName,
  });
});
