import { Router } from "express";

export const bookRouter = Router();
bookRouter.route("/").post((req, res) => {
  res.json({
    // bookDetails: `${bookName} author name is ${author} and it cost  NRs ${cost} `,
    bookDetails: `${req.body.bookName} author name is ${req.body.author} and it cost NRs ${req.body.cost}`,
  });
});

// learn math author name is nitan and it cost  NRs 1234
