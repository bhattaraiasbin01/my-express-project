import { Router } from "express";

export const firstRouter = Router();

firstRouter
  .route("/")
  .post((request, response) => {
    response.json("i am post");
  })
  .get((request, response) => {
    response.json("i am get");
  })
  .patch((request, response) => {
    response.json("i am patch");
  })
  .delete((request, response) => {
    response.json("i am delete");
  });
firstRouter
  .route("/name")
  .post((request, response) => {
    response.json("i am post name");
  })
  .get((request, response) => {
    response.json("i am get name");
  })
  .patch((request, response) => {
    response.json("i am patch name");
  })
  .delete((request, response) => {
    response.json("i am delete name");
  });
